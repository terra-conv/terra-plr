pub use terra_items::{Item, ItemSlot, Prefix, SingleItemSlot};
use terra_types::{NonNegativeI32, PositiveI32};

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub struct DpadShortcuts {
	pub up: Option<NonNegativeI32>,
	pub down: Option<NonNegativeI32>,
	pub left: Option<NonNegativeI32>,
	pub right: Option<NonNegativeI32>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct InventorySlot {
	pub item: Item,
	pub prefix: Option<Prefix>,
	pub count: PositiveI32,
	pub is_favorite: bool,
}

impl From<ItemSlot> for InventorySlot {
	fn from(value: ItemSlot) -> Self {
		InventorySlot {
			item: value.item,
			prefix: value.prefix,
			count: value.count,
			is_favorite: false,
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Default)]
pub struct ArmorRow {
	pub armor: Option<SingleItemSlot>,
	pub vanity_armor: Option<SingleItemSlot>,
	pub dye: Option<SingleItemSlot>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct AccessoryRow {
	pub accessory: Option<SingleItemSlot>,
	pub vanity_accessory: Option<SingleItemSlot>,
	pub dye: Option<SingleItemSlot>,
	pub is_accessory_shown: bool,
}

impl Default for AccessoryRow {
	fn default() -> Self {
		Self {
			accessory: None,
			vanity_accessory: None,
			dye: None,
			is_accessory_shown: true,
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Default)]
pub struct MiscRow {
	pub item: Option<SingleItemSlot>,
	pub dye: Option<SingleItemSlot>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct MiscRowWithVisibility {
	pub item: Option<SingleItemSlot>,
	pub dye: Option<SingleItemSlot>,
	pub is_shown: bool,
}

impl Default for MiscRowWithVisibility {
	fn default() -> Self {
		Self {
			item: None,
			dye: None,
			is_shown: true,
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Default)]
pub struct Loadout {
	pub helmet: ArmorRow,
	pub breastplate: ArmorRow,
	pub pants: ArmorRow,
	pub accessories: [AccessoryRow; 7],
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Default)]
pub struct Loadouts {
	pub loadouts: [Loadout; 3],
	pub selected_loadout_index: i32,
}
