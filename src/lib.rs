mod player;

use std::ops::RangeInclusive;

pub use player::{
	buff::{Buff, BuffEffect},
	default,
	error::{Error, Result},
	inventory,
	looks::{HairDye, Style},
	read, write, Difficulty, Player, SpawnPoint, Visibility, SUPPORTED_VERSIONS,
};
pub use terra_types::{Color, InTiles, NonNegativeI32, PositiveI32, Vec2};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TerrariaFileType {
	Unknown,
	Map,
	World,
	Player,
}

impl TerrariaFileType {
	pub fn name(self) -> &'static str {
		match self {
			Self::Map => "map",
			Self::Player => "player",
			Self::World => "world",
			Self::Unknown => "unknown",
		}
	}
}

impl TerrariaFileType {
	pub fn to_id(self) -> Option<u8> {
		Some(match self {
			Self::Map => 1,
			Self::World => 2,
			Self::Player => 3,
			Self::Unknown => return None,
		})
	}

	pub fn from_id(id: u8) -> Self {
		match id {
			1 => Self::Map,
			2 => Self::World,
			3 => Self::Player,
			_ => Self::Unknown,
		}
	}
}

fn round_division_i64(dividend: i64, divisor: i64) -> i64 {
	let double_remainder = (dividend % divisor) * 2;
	dividend / divisor
		+ if double_remainder.abs() >= divisor.abs() {
			if dividend.is_negative() == divisor.is_negative() {
				1
			} else {
				-1
			}
		} else {
			0
		}
}

fn round_division_i128(dividend: i128, divisor: i128) -> i128 {
	let double_remainder = (dividend % divisor) * 2;
	dividend / divisor
		+ if double_remainder.abs() >= divisor.abs() {
			if dividend.is_negative() == divisor.is_negative() {
				1
			} else {
				-1
			}
		} else {
			0
		}
}

fn remap(v: f32, x: RangeInclusive<f32>, y: RangeInclusive<f32>) -> f32 {
	if v <= *x.start() {
		return *y.start();
	}

	if v >= *x.end() {
		return *y.end();
	}

	let a = (*y.end() - *y.start()) / (*x.end() - *x.start());
	v * a + *y.start() - a * *x.start()
}
