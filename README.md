# terra-plr

Library for reading and modifying terraria player files.

## Examples

Change player max life to 300
```rust
let mut player = Player::read_player(File::open("player.plr")?)?;
player.max_life = 300;
player.write_player(File::create("player.plr")?, *SUPPORTED_VERSIONS.end())?;
```

Set 999 dirt items to first inventory slot
```rust
let mut player = Player::read_player(File::open("player.plr")?)?;
player.inventory[0][0] = Some(InventorySlot {
    item: terra_items::Item::DirtBlock,
    prefix: None,
    count: PositiveI32::new(999).expect("In bounds"),
    is_favorite: false,
});

player.write_player(File::create("player.plr")?, *SUPPORTED_VERSIONS.end())?;
```

Print player name
```rust
let mut player = Player::read_player(File::open("player.plr")?)?;
println!("{}", player.name);
```


## License

[AGPL v3+](LICENSE) © Filip K.
