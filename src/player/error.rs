use crate::TerrariaFileType;
use cs_datetime_parse::{OutOfRangeError, ParseError};
use std::{io, ops::RangeInclusive};
use thiserror::Error;

pub type Result<T> = core::result::Result<T, Error>;

fn format_u8_array(array: &[u8]) -> String {
	let mut text = String::with_capacity(array.len());
	for byte in array {
		if !byte.is_ascii_graphic() {
			text.push_str("\\x");
			text.push_str(&byte.to_string());
			continue;
		}

		let char = *byte as char;
		if char == '\\' {
			text.push_str("\\\\");
			continue;
		}

		text.push(char);
	}

	text
}

#[derive(Debug, Error)]
pub enum Error {
	#[error("the version {version} is not supported, supported versions: {version_bounds:?}")]
	UnsupportedVersion {
		version: i32,
		version_bounds: RangeInclusive<i32>,
	},

	#[error("invalid magic number expected b\"{}\", found b\"{}\"", format_u8_array(.found), format_u8_array(.expected))]
	InvalidMagicNumber { found: Vec<u8>, expected: Vec<u8> },

	#[error("invalid file type expected {}, found {}", .expected.name(), .found.name())]
	InvalidFileType {
		found: TerrariaFileType,
		expected: TerrariaFileType,
	},

	#[error("io error")]
	Io(#[from] io::Error),

	#[error("{0}")]
	DateTimeCsOutOfRange(#[from] OutOfRangeError),

	#[error("{0}")]
	DateTimeParse(#[from] ParseError),
}
