#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum HairDye {
	Life = 1,
	Mana = 2,
	Depth = 3,
	Money = 4,
	Time = 5,
	Team = 6,
	Biome = 7,
	Party = 8,
	Rainbow = 9,
	Speed = 10,
	Martian = 11,
	Twilight = 12,
}

impl HairDye {
	pub fn to_id(self) -> u8 {
		self as u8
	}

	pub fn from_id(id: u8) -> Option<Self> {
		Some(match id {
			1 => HairDye::Life,
			2 => HairDye::Mana,
			3 => HairDye::Depth,
			4 => HairDye::Money,
			5 => HairDye::Time,
			6 => HairDye::Team,
			7 => HairDye::Biome,
			8 => HairDye::Party,
			9 => HairDye::Rainbow,
			10 => HairDye::Speed,
			11 => HairDye::Martian,
			12 => HairDye::Twilight,
			_ => return None,
		})
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Style {
	MaleStarter = 0,
	MaleSticker = 1,
	MaleGangster = 2,
	MaleCoat = 3,
	FemaleStarter = 4,
	FemaleSticker = 5,
	FemaleGangster = 6,
	FemaleCoat = 7,
	MaleDress = 8,
	FemaleDress = 9,
	MaleDisplayDoll = 10,
	FemaleDisplayDoll = 11,
}

impl Style {
	pub fn to_id(self, version: i32) -> u8 {
		if version < 161 && self == Self::FemaleDress {
			return 7;
		}
		self as u8
	}

	pub fn from_id(id: u8, version: i32) -> Option<Self> {
		Some(match id {
			0 => Self::MaleStarter,
			1 => Self::MaleSticker,
			2 => Self::MaleGangster,
			3 => Self::MaleCoat,
			4 => Self::FemaleStarter,
			5 => Self::FemaleSticker,
			6 => Self::FemaleGangster,
			7 if version < 161 => Self::FemaleDress,
			7 => Self::FemaleCoat,
			8 => Self::MaleDress,
			9 => Self::FemaleDress,
			10 => Self::MaleDisplayDoll,
			11 => Self::FemaleDisplayDoll,
			_ => return None,
		})
	}

	pub fn is_male_style(self) -> bool {
		match self {
			Self::MaleStarter
			| Self::MaleSticker
			| Self::MaleGangster
			| Self::MaleCoat
			| Self::MaleDress
			| Self::MaleDisplayDoll => true,

			Self::FemaleStarter
			| Self::FemaleSticker
			| Self::FemaleGangster
			| Self::FemaleCoat
			| Self::FemaleDress
			| Self::FemaleDisplayDoll => false,
		}
	}
}
